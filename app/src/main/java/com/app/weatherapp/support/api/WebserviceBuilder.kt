package com.leca.installer.api

import com.app.weatherapp.support.model.WeatherAPIResponse
import io.reactivex.Observable
import retrofit2.http.*


/**
 * Declare all the APIs in this class with specific interface
 * e.g. Profile for Login/Register Apis
 */
interface WebserviceBuilder {


    @GET("weather")
    fun getWeather(
        @Query("lat") lat: String?,
        @Query("lon") lon: String?,
        @Query("appid") appid: String? = "fae7190d7e6433ec3a45285ffcf55c86"
    ): Observable<WeatherAPIResponse>


}