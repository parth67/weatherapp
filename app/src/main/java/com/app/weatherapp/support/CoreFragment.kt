package com.app.weatherapp.support

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IntRange
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.weatherapp.support.events.Event
import com.app.weatherapp.support.ktx.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

abstract class CoreFragment<VM : ActivityViewModel, DB : ViewDataBinding> : Fragment() {

    protected lateinit var viewModel: VM
    private lateinit var binding: DB
    private var reference = false
    protected abstract val isBackEnabled: Boolean
    protected abstract val title: String
    protected var coreActivityVM: ActivityViewModel? = null
    private val PERMISSION_CODE = 101
    private var permissionCallBack: PermissionCallBack? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::binding.isInitialized) {
            binding = DataBindingUtil.inflate(inflater, getLayout(), container, false)
            activity?.let {
                coreActivityVM = ViewModelProvider(it).get(ActivityViewModel::class.java)
            }
            binding.setLifecycleOwner(this);
            viewModel = ViewModelProvider(this).get(createViewModel())
            setVM(binding)
        } else {
            val v = binding.root.parent as ViewGroup?
            v?.removeView(binding.root)
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        initData()
        if (!reference) {
            createReference()
            reference = true
        }
        coreActivityVM?.isEmpty?.value = false
        coreActivityVM?.isBackEnabled?.value = isBackEnabled
        coreActivityVM?.title?.set(title)
    }

    private fun initData() {
//        viewModel.showContent.observe(this, Observer {
//            activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
//            if (it == true) {
//                stateLayout?.content()
//            }
//        })
//
//        viewModel.isLoading.observe(this, Observer {
//            if (stateLayout?.isLoadingVisible() == false) {
//                activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
//                if (it == false) {
//                    stateLayout?.loading()
//                } else {
//                    stateLayout?.loadingWithContent()
//                }
//            }
//        })
//
//        viewModel.networkError.observe(this, Observer {
//            activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
//            it?.let { it1 ->
//                stateLayout?.infoButtonListener(it1)
//            }
//        })
//
//        viewModel.infoStateview.observe(this, Observer {
//            activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
//            stateLayout?.showInfo(it)
//        })
//
//        viewModel.networkError.observe(this, Observer {
//            activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
//            it?.let { it1 ->
//                stateLayout?.infoButtonListener(it1)
//            }
//        })

        viewModel.toastMessage.observe(this, Observer {
            toastMessage(it)
        })

//        viewModel.snackMessage.observe(this, Observer {
//            snackMessage(it)
//        })

    }

    fun toastMessage(message: String?) {
        message?.let {
            toast(message)
        }
    }

//    fun snackMessage(message: String?) {
//        message?.let {
//            binding.root.apply {
//                snack(it) {}
//            }
//        }
//    }

    @LayoutRes
    abstract fun getLayout(): Int

    abstract fun createViewModel(): Class<out VM>

    abstract fun setVM(binding: DB)

    abstract fun createReference()

    fun getBinding(): DB = binding


    protected fun requestPermissionsIfRequired(
        permissions: ArrayList<String>,
        permissionCallBack: PermissionCallBack?
    ) {
        this.permissionCallBack = permissionCallBack
        if (checkSelfPermissions(permissions)) {
            permissionCallBack?.permissionGranted()
        } else {
            requestAllPermissions(permissions, PERMISSION_CODE)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults.none { it != PackageManager.PERMISSION_GRANTED }) {
                    permissionCallBack?.permissionGranted()
                } else {
                    if (checkPermissionRationale(permissions)) {
                        permissionCallBack?.permissionDenied()
                    } else {
                        permissionCallBack?.onPermissionDisabled()
                    }
                }
            }
        }
    }

    protected fun onBack() {
        activity?.onBackPressed()
    }

    protected fun onBackExclusive(fragment: Class<out Fragment>) {
        activity?.supportFragmentManager?.popBackStackImmediate(fragment.name, 0)
    }

    protected fun onBackInclusive(fragment: Class<out Fragment>) {
        activity?.supportFragmentManager?.popBackStackImmediate(
            fragment.name,
            FragmentManager.POP_BACK_STACK_INCLUSIVE
        )
    }

    protected fun onBack(@IntRange(from = 1, to = 100) steps: Int) {
        for (i in 1..steps) {
            activity?.supportFragmentManager?.popBackStack()
        }
    }

    fun post(data: Any) {
        EventBus.getDefault().post(data)
    }

    fun postSticky(data: Any) {
        EventBus.getDefault().postSticky(data)
    }

    fun repostSticky(data: Any) {
        EventBus.getDefault().removeAllStickyEvents()
        EventBus.getDefault().postSticky(data)
    }

    fun removeStickyEvent(data: Any) {
        EventBus.getDefault().removeStickyEvent(data)
    }

    @Subscribe
    open fun onEvent(event: Event) {
    }


    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }
}