package com.app.weatherapp.support.ktx

import android.content.Context
import androidx.annotation.StringRes
import com.app.weatherapp.R
import java.io.IOException

@StringRes
fun Throwable.getError(context: Context) : Int{
    var errorResId = R.string.error_unknown
    if (this is IOException){
        errorResId = if (context.isNetworkConnected()){
            R.string.error_connection_server
        } else {
            R.string.error_no_network
        }
    }else if (this is com.google.gson.JsonSyntaxException){
        errorResId = R.string.error_json_exception
    }
    return errorResId
}

fun Throwable.getErrorMessage(context: Context) : String{
    return context.string(getError(context))
}