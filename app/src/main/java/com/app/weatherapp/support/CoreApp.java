package com.app.weatherapp.support;

import android.app.Application;

import androidx.appcompat.app.AppCompatDelegate;

import com.app.weatherapp.support.utilise.AppForegroundChecker;

import io.realm.Realm;


public class CoreApp extends Application {

    private static CoreApp mInstance;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    AppForegroundChecker.Listener listener = new AppForegroundChecker.Listener() {
        public void onBecameForeground() {
//            Toaster.shortToast("foreground");
        }

        public void onBecameBackground() {
//            Toaster.shortToast("background");
        }
    };

    public static synchronized CoreApp getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        AppForegroundChecker.get(mInstance).addListener(listener);
        Realm.init(this);
    }

    @Override
    public void onTerminate() {
        AppForegroundChecker.get(getInstance()).removeListener(listener);
        super.onTerminate();
    }
}
