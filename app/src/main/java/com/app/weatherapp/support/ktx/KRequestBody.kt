package com.app.weatherapp.support.ktx

import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File
import okhttp3.MultipartBody


fun String.toRequestBody(): RequestBody {
    return RequestBody.create(MediaType.parse("text/plain"), this)
}

fun File.toRequestBodyImage(key: String): MultipartBody.Part {

    val filePart = MultipartBody.Part.createFormData(
        key,
        this.name,
        RequestBody.create(MediaType.parse("image/*"), this)
    )

    return filePart
}