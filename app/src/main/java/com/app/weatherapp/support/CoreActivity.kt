package com.app.weatherapp.support

import android.content.pm.PackageManager
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import com.app.weatherapp.support.events.ShowError
import com.app.weatherapp.support.ktx.*


abstract class CoreActivity<VM : ActivityViewModel, DB : ViewDataBinding> : AppCompatActivity() {


    lateinit var viewModel: VM
    private lateinit var binding: DB
    private val PERMISSION_CODE = 111
    private var permissionCallBack: PermissionCallBack? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, getLayout())
        viewModel = ViewModelProvider(this).get(createViewModel())
        binding.setLifecycleOwner(this);
        setVM(binding)
        createReference()
        initData()
    }

    private fun initData() {
//        viewModel.showContent.observe(this, Observer {
//            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
//            if (it == true) {
//                stateLayout?.content()
//            }
//        })
//
//        viewModel.isLoading.observe(this, Observer {
//            if (stateLayout?.isLoadingVisible() == false) {
//                window.addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
//                if (it == false) {
//                    stateLayout?.loading()
//                } else {
//                    stateLayout?.loadingWithContent()
//                }
//            }
//        })
//
//        viewModel.networkError.observe(this, Observer {
//            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
//            it?.let { it1 ->
//                stateLayout?.infoButtonListener(it1)
//            }
//        })
//
//        viewModel.infoStateview.observe(this, Observer {
//            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
//            stateLayout?.showInfo(it)
//        })
//
//        viewModel.networkError.observe(this, Observer {
//            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
//            it?.let { it1 ->
//                stateLayout?.infoButtonListener(it1)
//            }
//        })

        viewModel.toastMessage.observe(this, Observer {
            toastMessage(it)
        })

//        viewModel.snackMessage.observe(this, Observer {
//            snackMessage(it)
//        })

    }

    fun toastMessage(message: String?) {
        message?.let {
            toast(message)
        }
    }

//    fun snackMessage(message: String?) {
//        message?.let {
//            binding.root.apply {
//                snack(it) {}
//            }
//        }
//    }

    @LayoutRes
    abstract fun getLayout(): Int

    abstract fun createViewModel(): Class<out VM>

    abstract fun setVM(binding: DB)

    abstract fun createReference()

    fun getBinding(): DB = binding


    override fun onResume() {
        super.onResume()
        overridePendingTransition(0, 0)
        EventBus.getDefault().register(this)
    }

    override fun onPause() {
        super.onPause()
        EventBus.getDefault().unregister(this)
    }

    fun post(data: Any) {
        EventBus.getDefault().post(data)
    }

    fun postSticky(data: Any) {
        EventBus.getDefault().postSticky(data)
    }

    fun removeStickyEvent(data: Any) {
        EventBus.getDefault().removeStickyEvent(data)
    }

    var isShowing = false

    @Subscribe
    fun showError(error: ShowError) {
//        if (!isShowing) {
//            isShowing = true
        toastMessage(error.error) //{ isShowing = false }
//        }
        //binding.root.snackBar(error.error)
    }

    protected fun requestPermissionsIfRequired(
        permissions: ArrayList<String>,
        permissionCallBack: PermissionCallBack?
    ) {
        this.permissionCallBack = permissionCallBack
        if (checkSelfPermissions(permissions)) {
            permissionCallBack?.permissionGranted()
        } else {
            requestAllPermissions(permissions, PERMISSION_CODE)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults.none { it != PackageManager.PERMISSION_GRANTED }) {
                    permissionCallBack?.permissionGranted()
                } else {
                    if (checkPermissionRationale(permissions)) {
                        permissionCallBack?.permissionDenied()
                    } else {
                        permissionCallBack?.onPermissionDisabled()
                    }
                }
            }
        }
    }

    /*override fun onBackPressed() {
        if (viewModel.progressBar.get() == View.GONE)
            super.onBackPressed()
    }*/
}
