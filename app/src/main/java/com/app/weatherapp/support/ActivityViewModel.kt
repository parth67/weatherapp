package com.app.weatherapp.support

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class ActivityViewModel : ViewModel() {

    val title = ObservableField<String>()
    val isBackEnabled = MutableLiveData<Boolean>()
    val isEmpty = MutableLiveData<Boolean>()
    val toolbarTitle = MutableLiveData<String>()

    val showContent = MutableLiveData<Boolean>()
    val isLoading = MutableLiveData<Boolean>()
    val networkError = MutableLiveData<() -> Unit>()

    val toastMessage = MutableLiveData<String>()
    val snackMessage = MutableLiveData<String>()

    val DEVICETYPE: String = ""+1

    fun showToast(message: String?){
        toastMessage.value = message
    }

    fun showSnackBar(message: String?){
        snackMessage.value = message
    }

    fun content(content: Boolean = true) {
        showContent.value = content
    }

    fun loading(withContent: Boolean = true) {
        isLoading.value = withContent
    }

    fun toolbarTitle(string: String) {
        toolbarTitle.value = string
    }
}