package com.app.weatherapp.support.ktx

import java.text.SimpleDateFormat
import java.util.*


fun String.getTime(): String {

    try {
        val date = Date(this.toLong())
        val formatter = SimpleDateFormat("HH:mm")
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"))
        return formatter.format(date)
    } catch (e: Exception) {
        e.printStackTrace()
        return this
    }
}