package com.app.weatherapp.support.model

data class WeatherAPIResponse(
    val base: String,
    val clouds: Clouds,
    val cod: String,
    val coord: Coord,
    val dt: Long,
    val id: Int,
    val main: Main,
    val name: String,
    val sys: Sys,
    val timezone: String,
    val visibility: String,
    val weather: List<Weather>,
    val wind: Wind
)

data class Clouds(
    val all: String
)

data class Coord(
    val lat: String,
    val lon: String
)

data class Main(
    val feels_like: String,
    val humidity: String,
    val pressure: String,
    val temp: String,
    val temp_max: String,
    val temp_min: String
)

data class Sys(
    val id: String,
    val sunrise: String,
    val sunset: String,
    val type: String
)

data class Weather(
    val description: String,
    val icon: String,
    val id: String,
    val main: String
)

data class Wind(
    val deg: String,
    val speed: Double
)