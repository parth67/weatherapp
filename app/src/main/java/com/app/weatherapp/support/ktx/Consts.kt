package com.app.weatherapp.support.ktx

import android.annotation.SuppressLint
import android.content.Context
import android.location.LocationManager
import android.provider.Settings
import android.view.View
import org.greenrobot.eventbus.EventBus

const val MAINTENANCE_END_TIME = "end_time"

const val ONLOGOUT = "3"

const val SHOW_PROGRESS = "1"

const val DISMISS_PROGRESS = "0"

const val USER_TYPE = "user_type"
const val USER_RETAILER = "Retailer"
const val USER_MECHANIC = "Mechanic"
const val USER_CVL_MECHANIC = "CVL Mechanic"
const val USER_MCO_MECHANIC = "MCO Mechanic"
const val USER_BUSINESS_USER = "HO"
const val MOBILE_NUMBER = "mobile_number"
const val USERNAME = "user_name"
const val FROM_SCREEN = "from_screen"
const val RESET_PASSWORD = "reset_password"
const val OTP = "otp"
const val PARTICIPANT_ID = "participant_id"
const val BALANCE_POINTS = "balance_points"

fun Any.showProgress() = EventBus.getDefault().postSticky(SHOW_PROGRESS)

fun Any.dismissProgress() = EventBus.getDefault().postSticky(DISMISS_PROGRESS)

@SuppressLint("HardwareIds")
fun Context.getDeviceId(): String =
    Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID) ?: ""

fun Boolean.getVisibility() = if (this) View.VISIBLE else View.GONE

fun Context.getGPSEnable(): Boolean {

    var lm = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager

    var gps_enabled = false;
    var network_enabled = false;

    try {
        gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
    } catch (ex: Exception) {
        ex.printStackTrace()
    }

    try {
        network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    } catch (ex: Exception) {
        ex.printStackTrace()
    }

    return (gps_enabled || network_enabled)
}

