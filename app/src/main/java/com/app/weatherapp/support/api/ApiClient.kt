package com.app.weatherapp.support.api

import com.google.gson.GsonBuilder
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object ApiClient {

    private val OKHTTP_TIMEOUT = 30 * 10 // seconds
    private var retrofit: Retrofit? = null
    private var retrofitHeader: Retrofit? = null
    private lateinit var okHttpClient: OkHttpClient
    const val BUILD_TYPE_DEBUG = true

    /**
     * Staging Test Url
     **/
//    const val BASE_URL = "http://php.dev.drcsystems.ooo/php-projects/leca/api/v1/"
//    const val ContactUs =
//        "http://php.dev.drcsystems.ooo/php-projects/leca/site/cms?name=contactus"


    /**
     * UAT Test Url
     **/
    const val BASE_URL = "http://api.openweathermap.org/data/2.5/"

    /**
     * @return [Retrofit] object its single-tone
     */
    fun clear() {
        retrofit = null
        retrofitHeader = null
    }

    fun getApiClient(): Retrofit {
        if (retrofit == null) {
            val gson = GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create()

            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(getOKHttpClient())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
        }
        return retrofit!!
    }


    /**
     * settings like caching, Request Timeout, Logging can be configured here.
     *
     * @return [OkHttpClient]
     */
    private fun getOKHttpClient(): OkHttpClient {
        return if (!ApiClient::okHttpClient.isInitialized) {
            val builder = OkHttpClient.Builder()
                .retryOnConnectionFailure(true)
                .connectTimeout(OKHTTP_TIMEOUT.toLong(), TimeUnit.SECONDS)
                .writeTimeout(OKHTTP_TIMEOUT.toLong(), TimeUnit.SECONDS)
                .readTimeout(OKHTTP_TIMEOUT.toLong(), TimeUnit.SECONDS)

            if (BUILD_TYPE_DEBUG) {
                val loggingInterceptor = HttpLoggingInterceptor()
                loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                builder.addInterceptor(loggingInterceptor)
            }

            builder.addInterceptor { chain ->
                val requestBuilder = chain.request().newBuilder()
                requestBuilder.header("Content-Type", "application/json")
                requestBuilder.header("access-token", "")
                chain.proceed(requestBuilder.build())
            }
            okHttpClient = builder.build()
            okHttpClient
        } else {
            okHttpClient
        }
    }
}

interface SingleCallback<T> {
    /**
     * @param o        Whole response Object
     * @param apiNames [A] to differentiate Apis
     */
    fun onSingleSuccess(o: T)

    /**
     * @param throwable returns [Throwable] for checking Exception
     * @param apiNames  [A] to differentiate Apis
     */
    fun onFailure(throwable: Throwable)

    fun onError(message: String?)
}

fun <T> subscribeToSingle(observable: Observable<T>, singleCallback: SingleCallback<T>?) {
    Single.fromObservable(observable)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(object : SingleObserver<T> {
            override fun onSuccess(t: T) {
                try {
                    singleCallback?.onSingleSuccess(t)
//
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onSubscribe(d: Disposable) {

            }

            override fun onError(e: Throwable) {

                when (e) {
                    is HttpException -> {
                        singleCallback?.onFailure(e)
                    }
                    else -> singleCallback?.onFailure(e)
                }
            }
        })
}