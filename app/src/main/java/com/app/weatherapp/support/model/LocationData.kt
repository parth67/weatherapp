package com.app.weatherapp.support.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


open class LocationData(
    @PrimaryKey
    var id: Long = 0,
    var place: String? = "",
    var latitude: Double? = 0.0,
    var longtite: Double? = 0.0
) : RealmObject() {
}