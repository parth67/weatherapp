package com.app.weatherapp.support.ktx

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment

inline fun <reified T : Activity> Activity.startActivity(bundle: Bundle) =
    startActivity(Intent(this, T::class.java).putExtras(bundle))

inline fun <reified T : Activity> Activity.startActivity() = startActivity(Intent(this, T::class.java))

inline fun <reified T : Activity> Activity.startActivityForResult(requestCode: Int) = startActivityForResult(Intent(this, T::class.java), requestCode)

inline fun <reified T : Activity> Fragment.startActivity() = startActivity(Intent(activity, T::class.java))

fun Context.openPlayStore() {
    val appPackageName = packageName // getPackageName() from Context or Activity object
    try {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
    } catch (anfe: android.content.ActivityNotFoundException) {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
    }
}