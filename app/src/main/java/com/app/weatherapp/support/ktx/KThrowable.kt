package com.app.weatherapp.support.ktx

import androidx.annotation.StringRes
import com.app.weatherapp.support.CoreApp
import org.greenrobot.eventbus.EventBus
import com.app.weatherapp.support.events.ShowError
import com.app.weatherapp.support.ktx.DISMISS_PROGRESS

fun Throwable.postError() = EventBus.getDefault().postSticky(ShowError("$message"))

fun Throwable.postError(error: String) {
    EventBus.getDefault().postSticky(DISMISS_PROGRESS)
    EventBus.getDefault().postSticky(ShowError(error))
}

fun Throwable.postError(@StringRes error: Int) {
    EventBus.getDefault().postSticky(DISMISS_PROGRESS)
    EventBus.getDefault().postSticky(ShowError(error = CoreApp.getInstance().getString(error)))
}

fun postError(error: String) = EventBus.getDefault().postSticky(ShowError(error))

fun postError(@StringRes error: Int) = EventBus.getDefault().postSticky(ShowError(error = CoreApp.getInstance().getString(error)))
