package com.app.weatherapp.support.ktx

import android.content.Context
import android.content.DialogInterface
import androidx.annotation.ArrayRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import com.app.weatherapp.R

/**
 * Created by jayeshparkariya on 28/2/18.
 */
fun Context.simpleAlert(msg: String, positiveButton: (() -> Unit)? = null) {

    val mDialog: AlertDialog.Builder = AlertDialog.Builder(this)
    mDialog.setTitle(getString(R.string.app_name))
        .setMessage(msg)
        .setPositiveButton(getString(R.string.ok)) { _: DialogInterface, _: Int ->
            positiveButton?.invoke()
        }
    mDialog.create().show()
}

fun Context.simpleAlert(msg: String, positiveText: String, positiveButton: (() -> Unit)? = null) {

    val mDialog: AlertDialog.Builder = AlertDialog.Builder(this)
    mDialog.setTitle(getString(R.string.app_name))
        .setMessage(msg)
        .setPositiveButton(positiveText) { _: DialogInterface, _: Int ->
            positiveButton?.invoke()
        }
        .setCancelable(false)
    mDialog.create().show()
}

fun Context.simpleAlert(
    title: String, msg: String, btnTitle: String, positiveButton: (() -> Unit)? = null
) {
    val mDialog: AlertDialog.Builder = AlertDialog.Builder(this)
    mDialog.setTitle(title)
        .setMessage(msg)
        .setPositiveButton(getString(R.string.ok)) { _: DialogInterface, _: Int ->
            positiveButton?.invoke()
        }
    mDialog.create().show()
}

fun Context.appForceUpdate(
    title: String,
    msg: String,
    btnTitle: String,
    positiveButton: (() -> Unit)? = null
) {
    val mDialog: AlertDialog.Builder = AlertDialog.Builder(this)
    mDialog.setTitle(title)
        .setMessage(msg)
        .setPositiveButton(getString(R.string.ok)) { _: DialogInterface, _: Int ->
            positiveButton?.invoke()
        }
    mDialog.create().show()
}

fun Context.confirmationDialog(
    msg: String,
    btnPositiveClick: (() -> Unit)? = null,
    btnNegativeClick: (() -> Unit)? = null
) {
    val mDialog: AlertDialog.Builder = AlertDialog.Builder(this)
    mDialog.setTitle(getString(R.string.app_name))
        .setMessage(msg)
        .setNegativeButton(getString(R.string.no)) { _, which -> btnNegativeClick?.invoke() }
        .setPositiveButton(getString(R.string.yes)) { dialg, which -> btnPositiveClick?.invoke() }
        .create().show()
}

fun Context.confirmationDialog(
    title: String,
    msg: String,
    btnPositiveClick: (() -> Unit)? = null,
    btnNegativeClick: (() -> Unit)? = null
) {
    val mDialog: AlertDialog.Builder = AlertDialog.Builder(this)
    mDialog.setTitle(title)
        .setMessage(msg)
        .setNegativeButton(getString(R.string.no)) { _, which -> btnNegativeClick?.invoke() }
        .setPositiveButton(getString(R.string.yes)) { dialg, which -> btnPositiveClick?.invoke() }
        .create().show()
}

fun Context.confirmationDialog(
    title: String,
    msg: String,
    btnPositive: String,
    btnNegative: String,
    btnPositiveClick: (() -> Unit)? = null,
    btnNegativeClick: (() -> Unit)? = null,
    icon: Int? = R.mipmap.ic_launcher
) {
    val mDialog: AlertDialog.Builder = AlertDialog.Builder(this)
    mDialog.setTitle(title)
        .setMessage(msg)
        .setNegativeButton(getString(R.string.no)) { _, which -> btnNegativeClick?.invoke() }
        .setPositiveButton(getString(R.string.yes)) { dialg, which -> btnPositiveClick?.invoke() }
        .create().show()
}

fun Context.takePick(onGallery: (() -> Unit)? = null, onCamera: (() -> Unit)? = null) {
    val mDialog: AlertDialog.Builder = AlertDialog.Builder(this)
    mDialog.setTitle("Select Picture")
        .setItems(R.array.select_image_from) { dialogInterface, which ->
            when (which) {
                0 -> {
                    dialogInterface.dismiss()
                    /**Open gallery*/
                    onGallery?.invoke()
                }
                1 -> {
                    dialogInterface.dismiss()
                    /**Open camera*/
                    onCamera?.invoke()
                }
            }
        }
        .create().show()
}

fun Context.fileExplorer(
    onGallery: (() -> Unit)? = null,
    onCamera: (() -> Unit)? = null,
    onDocument: (() -> Unit)? = null
) {
    val mDialog: AlertDialog.Builder = AlertDialog.Builder(this)
    mDialog.setTitle("Select Picture")
        .setItems(R.array.select_file_from) { dialogInterface, which ->
            when (which) {
                0 -> {
                    dialogInterface.dismiss()
                    /**Open gallery*/
                    onGallery?.invoke()
                }
                1 -> {
                    dialogInterface.dismiss()
                    /**Open camera*/
                    onCamera?.invoke()
                }
                2 -> {
                    dialogInterface.dismiss()
                    /**Open document*/
                    onDocument?.invoke()
                }
            }
        }
        .create().show()
}


fun Context.accountTypes(onItemSelected: ((item: String) -> Unit)? = null) {
    val mDialog: AlertDialog.Builder = AlertDialog.Builder(this)
    val data = resources.getStringArray(R.array.accountTypes)
    mDialog.setTitle("Account Type")
        .setItems(data) { dialogInterface, which ->
            dialogInterface.dismiss()
            onItemSelected?.invoke(data[which])
        }
        .create().show()
}

fun Context.showListDialog(
    title: String?,
    list: ArrayList<String?>,
    onItemSelected: ((item: String?, position: Int) -> Unit)? = null
) {
    val mDialog: AlertDialog.Builder = AlertDialog.Builder(this)
    val data = list.toTypedArray()
    mDialog.setTitle(title)
        .setItems(data) { dialogInterface, which ->
            dialogInterface.dismiss()
            onItemSelected?.invoke(data[which], which)
        }
        .create().show()
}

//fun Context.showListDialog(
//    title: String?,
//    data: Array<String>,
//    onItemSelected: ((item: String,position: Int) -> Unit)? = null
//) {
//    val mDialog: AlertDialog.Builder = AlertDialog.Builder(this)
//    mDialog.setTitle(title)
//        .setItems(data) { dialogInterface, which ->
//            dialogInterface.dismiss()
//            onItemSelected?.invoke(data[which],which)
//        }
//        .create().show()
//}

fun Context.showListDialog(
    @StringRes title: Int,
    @ArrayRes list: Int,
    onItemSelected: ((item: String) -> Unit)? = null
) {
    val mDialog: AlertDialog.Builder = AlertDialog.Builder(this)
    val data = resources.getTextArray(list)
    mDialog.setTitle(title)
        .setItems(list) { dialogInterface, which ->
            dialogInterface.dismiss()
            onItemSelected?.invoke(data[which] as String)
        }
        .create().show()
}

fun Context.showListDialog(
    @StringRes title: Int, data: Array<String>,
    onItemSelected: ((item: String, which: Int) -> Unit)? = null
) {
    val mDialog: AlertDialog.Builder = AlertDialog.Builder(this)
    mDialog.setTitle(title)
        .setItems(data) { dialogInterface, which ->
            dialogInterface.dismiss()
            onItemSelected?.invoke(data[which], which)
        }
        .create().show()
}


fun <T> Context.showCustomListDialog(
    title: String?,
    list: ArrayList<T>,
    onItemSelected: ((item: T, position: Int) -> Unit)? = null
) {
    val mDialog: AlertDialog.Builder = AlertDialog.Builder(this)
    val data: Array<String?> = arrayOfNulls(list.size)
    list.forEachIndexed { index, t -> data[index] = t.toString() }
    mDialog.setTitle(title)
        .setItems(data) { dialogInterface, which ->
            dialogInterface.dismiss()
            onItemSelected?.invoke(list[which], which)
        }
        .create().show()
}

fun <T> Context.showCustomListDialog(
    @StringRes title: Int, list: ArrayList<T>,
    onItemSelected: ((item: T) -> Unit)? = null
) {
    val mDialog: AlertDialog.Builder = AlertDialog.Builder(this)
    val data: Array<String?> = arrayOfNulls(list.size)
    list.forEachIndexed { index, t -> data[index] = t.toString() }
    mDialog.setTitle(title)
        .setItems(data) { dialogInterface, which ->
            dialogInterface.dismiss()
            onItemSelected?.invoke(list[which])
        }
        .create().show()
}
