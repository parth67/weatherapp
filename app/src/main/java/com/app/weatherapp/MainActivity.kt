package com.app.weatherapp

import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.lifecycle.Observer
import com.app.weatherapp.databinding.ActivityMainBinding
import com.app.weatherapp.support.ActivityViewModel
import com.app.weatherapp.support.CoreActivity
import com.app.weatherapp.support.ktx.DISMISS_PROGRESS
import com.app.weatherapp.support.ktx.SHOW_PROGRESS
import kotlinx.android.synthetic.main.activity_main.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class MainActivity : CoreActivity<ActivityViewModel, ActivityMainBinding>() {
    override fun getLayout(): Int {
        return R.layout.activity_main
    }

    override fun createViewModel(): Class<out ActivityViewModel> {
        return ActivityViewModel::class.java
    }

    override fun setVM(binding: ActivityMainBinding) {
        binding.vm = viewModel
        binding.executePendingBindings()
    }

    override fun createReference() {

        setToolBar()
    }

    /**
     * setting the toolbar
     */
    private fun setToolBar() {
        setSupportActionBar(mToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        mToolbar?.setNavigationIcon(R.drawable.ic_arrow_back_24)
//
        viewModel.isBackEnabled.observe(this, Observer { it ->
            it?.let {
                if (it) {
                    mToolbar?.setNavigationIcon(R.drawable.ic_arrow_back_24)
                } else {
                    mToolbar?.setNavigationIcon(null)
                }
            }
        })

    }


    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    open fun showDialog(show: String) {
        when (show) {
            SHOW_PROGRESS -> {
                window.addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                progressBar.visibility = View.VISIBLE
            }
            DISMISS_PROGRESS -> {
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                progressBar.visibility = View.GONE
            }
        }
        EventBus.getDefault().removeStickyEvent(show)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}