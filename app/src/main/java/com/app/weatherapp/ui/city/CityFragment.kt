package com.app.weatherapp.ui.city


import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.app.weatherapp.R
import com.app.weatherapp.databinding.FragmentCityBinding
import com.app.weatherapp.databinding.FragmentDashboardBinding
import com.app.weatherapp.databinding.RowCitylistBinding
import com.app.weatherapp.support.CoreFragment
import com.app.weatherapp.support.adapters.BaseAdapter
import com.app.weatherapp.support.adapters.setUpRecyclerView
import com.app.weatherapp.support.ktx.getTime
import com.app.weatherapp.support.ktx.isNetworkConnected
import com.app.weatherapp.support.model.LocationData
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_dashboard.*
import org.greenrobot.eventbus.Subscribe
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class CityFragment : CoreFragment<CityVM, FragmentCityBinding>() {


    override val isBackEnabled: Boolean
        get() = true

    override val title: String
        get() = getString(R.string.city)

    override fun getLayout(): Int {
        return R.layout.fragment_city
    }

    override fun createViewModel(): Class<out CityVM> {
        return CityVM::class.java
    }

    override fun setVM(binding: FragmentCityBinding) {
        binding.vm = viewModel
        binding.executePendingBindings()
    }

    override fun createReference() {
    }


    @Subscribe(sticky = true)
    fun onReceive(data: LocationData) {


        viewModel.isInternetConnected.set(activity?.isNetworkConnected())

        if (viewModel.isInternetConnected.get() == true) {
            viewModel.address.set(data.place)
            viewModel.lat = "" + data.latitude
            viewModel.lon = "" + data.longtite
            callApi()
        }

        removeStickyEvent(
            data
        )


    }

    private fun callApi() {

        viewModel.getWeatherData()
            .observe(this,
                Observer {

                    viewModel.updatedAt.set(
                        "Updated at: " + SimpleDateFormat(
                            "dd/MM/yyyy hh:mm a",
                            Locale.ENGLISH
                        ).format(
                            Date(it.dt * 1000)
                        )
                    )

                    viewModel.temp.set("" + it.main.temp + "°C")
                    viewModel.tempMin.set("Min Temp: " + it.main.temp_min + "°C")
                    viewModel.tempMax.set("Max Temp: " + it.main.temp_max + "°C")

                    viewModel.pressure.set("" + it.main.pressure)
                    viewModel.humidity.set("" + it.main.humidity)

                    viewModel.sunrise.set(("" + it.sys.sunrise).getTime())
                    viewModel.sunset.set(("" + it.sys.sunset).getTime())
                    viewModel.windSpeed.set("" + it.wind.speed)

                    if (it.weather.isNotEmpty()) {
                        viewModel.weatherDescription.set("" + it.weather.get(0).description)
                    }
                })


    }
}
