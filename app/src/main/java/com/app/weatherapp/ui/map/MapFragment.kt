package com.app.weatherapp.ui.map


import android.content.pm.ActivityInfo
import android.location.Address
import android.location.Geocoder
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.app.weatherapp.R
import com.app.weatherapp.databinding.FragmentMapBinding
import com.app.weatherapp.support.CoreFragment
import com.app.weatherapp.support.ktx.confirmationDialog
import com.app.weatherapp.support.model.LocationData
import com.app.weatherapp.support.utilise.AppLog.TAG
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import io.realm.Realm
import io.realm.kotlin.createObject
import java.util.*


/**
 * A simple [Fragment] subclass.
 */

class MapFragment : CoreFragment<MapVM, FragmentMapBinding>(), OnMapReadyCallback,
    GoogleMap.OnMapClickListener {

    private var mMap: GoogleMap? = null
    override val isBackEnabled: Boolean
        get() = true
    override val title: String
        get() = getString(R.string.add_location)

    override fun getLayout(): Int {
        return R.layout.fragment_map
    }

    override fun createViewModel(): Class<out MapVM> {
        return MapVM::class.java
    }

    override fun setVM(binding: FragmentMapBinding) {
        binding.vm = viewModel
        binding.executePendingBindings()
    }

    override fun createReference() {

        val mapFragment = childFragmentManager?.findFragmentById(R.id.map) as SupportMapFragment

        mapFragment.getMapAsync(this)

    }

    override fun onResume() {
        super.onResume()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    override fun onPause() {
        super.onPause()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap

        val ltng = LatLng(23.0225, 72.5714)
        mMap?.addMarker(MarkerOptions().position(ltng).draggable(true).title("Ahmedabad"))
//        mMap?.moveCamera(CameraUpdateFactory.newLatLng(ltng))

        val cameraPosition =
            CameraPosition.Builder().target(ltng).zoom(12f).build()
        googleMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))


        mMap?.getUiSettings()?.setZoomControlsEnabled(true)

        mMap?.setOnMapClickListener(this);
    }

    override fun onMapClick(p0: LatLng) {
        mMap?.clear()

        Log.e(TAG, "" + p0?.latitude + p0?.longitude)

        val geocoder: Geocoder
        val addresses: List<Address>
        geocoder = Geocoder(context, Locale.getDefault())
        try {
            addresses = geocoder.getFromLocation(p0.latitude, p0.longitude, 1)
            val info = addresses[0].getAddressLine(0)

            viewModel.address = info
            viewModel.latLng = p0

            mMap?.addMarker(MarkerOptions().position(p0).draggable(true).title(info))
            mMap?.moveCamera(CameraUpdateFactory.newLatLng(p0))

            activity?.confirmationDialog(
                getString(R.string.are_you_sure_you_want_to_add) + " " + viewModel.address + " ?",
                btnPositiveClick = {
                    val realm = Realm.getDefaultInstance()

                    realm.executeTransaction { realm ->

                        val data = realm.createObject<LocationData>(Random().nextLong())

                        data.place = viewModel.address
                        data.latitude = viewModel.latLng?.latitude
                        data.longtite = viewModel.latLng?.longitude
                    }
                    findNavController().popBackStack()
                },
                btnNegativeClick = {

                })


        } catch (e: Exception) {
            e.printStackTrace()
            ""
        }
    }


}
