package com.app.weatherapp.ui.dashboard

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.app.weatherapp.support.ActivityViewModel
import com.google.android.gms.maps.model.LatLng

class DashboardVM : ActivityViewModel() {
    var isListEmpty: ObservableField<Boolean> = ObservableField(false)
}
