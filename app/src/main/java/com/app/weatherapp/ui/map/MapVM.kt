package com.app.weatherapp.ui.map

import com.app.weatherapp.support.ActivityViewModel
import com.google.android.gms.maps.model.LatLng

class MapVM : ActivityViewModel() {

    var address = ""
    var latLng : LatLng? = null

}