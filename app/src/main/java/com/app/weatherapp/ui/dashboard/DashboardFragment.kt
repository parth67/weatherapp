package com.app.weatherapp.ui.dashboard


import android.content.Context
import android.graphics.Canvas
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.app.weatherapp.R
import com.app.weatherapp.databinding.FragmentDashboardBinding
import com.app.weatherapp.databinding.RowCitylistBinding
import com.app.weatherapp.support.CoreFragment
import com.app.weatherapp.support.adapters.BaseAdapter
import com.app.weatherapp.support.adapters.setUpRecyclerView
import com.app.weatherapp.support.ktx.confirmationDialog
import com.app.weatherapp.support.model.LocationData
import io.realm.Realm
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator
import kotlinx.android.synthetic.main.fragment_dashboard.*


/**
 * A simple [Fragment] subclass.
 */
const val IS_FROM_DASHBOARD = "isFromDashboard"

class DashboardFragment : CoreFragment<DashboardVM, FragmentDashboardBinding>() {

    private var listAdapter: BaseAdapter<LocationData, RowCitylistBinding>? = null
    val list = ArrayList<LocationData>()

    override val isBackEnabled: Boolean
        get() = false

    override val title: String
        get() = getString(R.string.dashboard)

    override fun getLayout(): Int {
        return R.layout.fragment_dashboard
    }

    override fun createViewModel(): Class<out DashboardVM> {
        return DashboardVM::class.java
    }

    override fun setVM(binding: FragmentDashboardBinding) {
        binding.vm = viewModel
        binding.executePendingBindings()
    }

    override fun createReference() {


        floating_action_button.setOnClickListener {
            findNavController().navigate(R.id.mapFragment)

        }


        rvCityList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    floating_action_button.hide()
                } else {
                    floating_action_button.show()
                }
                super.onScrolled(recyclerView, dx, dy)
            }
        })

        val callback: ItemTouchHelper.SimpleCallback = object :
            ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                deleteRecord(viewHolder.adapterPosition)
            }

            override fun onChildDraw(
                c: Canvas,
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                dX: Float,
                dY: Float,
                actionState: Int,
                isCurrentlyActive: Boolean
            ) {

                RecyclerViewSwipeDecorator.Builder(
                    c,
                    recyclerView,
                    viewHolder,
                    dX,
                    dY,
                    actionState,
                    isCurrentlyActive
                )
                    .addBackgroundColor(
                        ContextCompat.getColor(
                            this@DashboardFragment.context as Context,
                            R.color.colorPrimary
                        )
                    )
                    .addSwipeLeftLabel("Delete")
                    .addSwipeLeftBackgroundColor(R.color.colorPrimaryDark)
                    .setSwipeLeftLabelTextSize(16, 16f)
                    .create()
                    .decorate()

                super.onChildDraw(
                    c,
                    recyclerView,
                    viewHolder,
                    dX,
                    dY,
                    actionState,
                    isCurrentlyActive
                )
            }
        }

        val itemTouchHelper = ItemTouchHelper(callback)
        itemTouchHelper.attachToRecyclerView(rvCityList)

        setList()

        listAdapter = rvCityList?.setUpRecyclerView(
            R.layout.row_citylist,
            list
        ) { item: LocationData, binder: RowCitylistBinding, position: Int ->
            binder.vm = item
            binder.root.setOnClickListener {


                findNavController().navigate(R.id.cityFragment)

                postSticky(item)
            }


            binder.executePendingBindings()
        }


    }

    private fun deleteRecord(position: Int) {

        val item = list.get(position)

        val realm = Realm.getDefaultInstance()

        var results = realm.where(LocationData::class.java).findAll()

        realm.executeTransaction { realm ->


            val obj = results.filter { it.id == item.id }

            if (obj.isNotEmpty()) {
                obj.get(0).deleteFromRealm()
                setList()
            }
        }

    }

    private fun setList() {

        list.clear()

        val realm = Realm.getDefaultInstance()

        val obj = realm.where(LocationData::class.java).findAll()

        list.addAll(realm.copyFromRealm(obj));

        viewModel.isListEmpty.set(list.isEmpty())

        listAdapter?.notifyDataSetChanged()

    }

    override fun onResume() {
        super.onResume()
        setList()
    }

}
