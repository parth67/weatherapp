package com.app.weatherapp.ui.city

import androidx.databinding.Observable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.app.weatherapp.support.ActivityViewModel
import com.app.weatherapp.support.CoreApp
import com.app.weatherapp.support.api.ApiClient
import com.app.weatherapp.support.api.SingleCallback
import com.app.weatherapp.support.api.subscribeToSingle
import com.app.weatherapp.support.ktx.dismissProgress
import com.app.weatherapp.support.ktx.getErrorMessage
import com.app.weatherapp.support.ktx.postError
import com.app.weatherapp.support.ktx.showProgress
import com.app.weatherapp.support.model.WeatherAPIResponse
import com.leca.installer.api.*
import java.text.SimpleDateFormat
import java.util.*

class CityVM : ActivityViewModel() {


    var lat = ""
    var lon = ""
    var address: ObservableField<String> = ObservableField("")
    var updatedAt: ObservableField<String> = ObservableField("")
    var temp: ObservableField<String> = ObservableField("")
    var tempMin: ObservableField<String> = ObservableField("")
    var tempMax: ObservableField<String> = ObservableField("")
    var pressure: ObservableField<String> = ObservableField("")
    var humidity: ObservableField<String> = ObservableField("")

    var sunrise: ObservableField<String> = ObservableField("")
    var sunset: ObservableField<String> = ObservableField("")
    var windSpeed: ObservableField<String> = ObservableField("")
    var weatherDescription: ObservableField<String> = ObservableField("")

    var isInternetConnected: ObservableField<Boolean> = ObservableField(true)

    fun getWeatherData(): MutableLiveData<WeatherAPIResponse> {
        showProgress()
        val data = MutableLiveData<WeatherAPIResponse>()
        subscribeToSingle(
            observable = ApiClient.getApiClient().create(WebserviceBuilder::class.java)
                .getWeather(
                    lat,
                    lon
                ),

            singleCallback = object : SingleCallback<WeatherAPIResponse> {
                override fun onSingleSuccess(o: WeatherAPIResponse) {
                    dismissProgress()
                    data.value = o
                }

                override fun onError(message: String?) {
                    dismissProgress()
//                    swipeRefresh.value = false
                    message?.let {
                        postError("$it")
                    }
//                    toastMessage.value = message
                }

                override fun onFailure(throwable: Throwable) {
                    dismissProgress()
//                    swipeRefresh.value = false
                    val message = throwable.getErrorMessage(CoreApp.getInstance())
                    message?.let {
                        postError("$it")
                    }
                }
            }

        )

        return data
    }

}
